use std::cmp::Ordering;
use std::fmt;
use std::rc::Rc;

#[derive(Eq, PartialEq, Clone, Copy)]
pub enum Direction {
    Left,
    Right,
    Up,
    Down,
}
impl fmt::Display for Direction {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        match &self {
            Direction::Left => write!(formatter, "L"),
            Direction::Right => write!(formatter, "R"),
            Direction::Up => write!(formatter, "U"),
            Direction::Down => write!(formatter, "D"),
        }
    }
}

#[derive(Eq, PartialEq, Clone, Hash)]
pub struct Puzzle {
    pub board: Vec<Vec<usize>>,
    pub positions: Vec<(usize, usize)>,
    pub open_pos: (usize, usize),
}

pub struct PatternDb {
    pub tiles: Vec<usize>,
    pub entries: Vec<u8>,
}

#[derive(Eq, PartialEq, Clone)]
pub struct PuzzleState {
    pub f_val: usize,
    pub puzzle: Puzzle,
    pub parent: Option<Rc<PuzzleState>>,
    pub move_from_parent: Option<Direction>,
    pub total_moves: usize,
}
impl Ord for PuzzleState {
    fn cmp(&self, other: &PuzzleState) -> Ordering {
        other.f_val.cmp(&self.f_val)
    }
}
impl PartialOrd for PuzzleState {
    fn partial_cmp(&self, other: &PuzzleState) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
