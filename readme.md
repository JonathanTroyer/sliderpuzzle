# Slider puzzle solver

This project uses the A* algorithm to solve a NxM slider puzzle,
written in [Rust](https://www.rust-lang.org/en-US/).